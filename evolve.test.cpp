#include "board.hpp"
#include "doctest.h"
#include "evolve.hpp"
#include "graph.hpp"

namespace plague {
TEST_CASE("testing evolve") {
  Board board1(40);
  board1(3, 7) = State::Infectious;
  board1(26, 8) = State::Infectious;
  board1(35, 20) = State::Infectious;
  Board newboard1 = board1;
  single_movement(newboard1, board1, 26, 8);
  bool moved1 = (newboard1.getState(25, 7) == State::Infectious ||
                 newboard1.getState(25, 8) == State::Infectious ||
                 newboard1.getState(25, 9) == State::Infectious ||
                 newboard1.getState(26, 7) == State::Infectious ||
                 newboard1.getState(26, 9) == State::Infectious ||
                 newboard1.getState(27, 7) == State::Infectious ||
                 newboard1.getState(27, 8) == State::Infectious ||
                 newboard1.getState(27, 9) == State::Infectious);
  if (moved1) {
    CHECK(newboard1.getState(26, 8) == State::Empty);
  } else {
    CHECK(newboard1.getState(26, 8) == State::Infectious);
  }

  Board board2(10);
  board2(0, 0) = State::Susceptible;
  Board newboard2 = board2;
  single_movement(newboard2, board2, 0, 0);
  bool moved = (newboard2.getState(0, 1) == State::Susceptible ||
                newboard2.getState(1, 0) == State::Susceptible ||
                newboard2.getState(1, 1) == State::Susceptible);
  if (moved1) {
    CHECK(newboard1.getState(26, 8) == State::Empty);
  } else {
    CHECK(newboard1.getState(26, 8) == State::Susceptible);
  }
  CHECK(newboard2.getState(-1, -1) == State::Recovered);
  CHECK(newboard2.getState(-1, 0) == State::Recovered);
  CHECK(newboard2.getState(-1, 1) == State::Recovered);
  CHECK(newboard2.getState(0, -1) == State::Recovered);
  CHECK(newboard2.getState(1, -1) == State::Recovered);

  Board board3;
  board3.random(board3, 80, 2000, 500, 100);
  Graph graph3;
  graph3.fill_graph(board3, 0);
  bool lockdown3 = false;
  Board newboard3 = board3;
  evolve(newboard3, lockdown3, 3, 0.4, 0, 2000, 0.2, 0, graph3, 0);
  CHECK(lockdown3 == true);

  Board board4;
  board4.random(board4, 60, 1000, 10, 100);
  Graph graph4;
  graph4.fill_graph(board4, 0);
  bool lockdown4 = true;
  evolve(board4, lockdown4, 2, 0, 0.6, 1000, 0.9, 0.8, graph4, 0);
  CHECK(lockdown4 == false);
  
  Board board5(35);
  board5(12, 10) = State::Infectious;
  board5(12, 11) = State::Susceptible;
  board5(13, 9) = State::Infectious;
  board5(13, 12) = State::Infectious;
  board5(14, 10) = State::Infectious;
  board5(14, 11) = State::Infectious;
  board5(14, 12) = State::Recovered;
  board5(15, 12) = State::Infectious;
  CHECK(nearby_infectious(board5, 13, 11, 2) == 6);
  CHECK(nearby_infectious(board5, 13, 11, 1) == 4);
}
} // namespace plague