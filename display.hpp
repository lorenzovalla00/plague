#ifndef PLAGUE_DISPLAY_HPP
#define PLAGUE_DISPLAY_HPP

#include "board.hpp"
#include "evolve.hpp"
#include "graph.hpp"
#include <SFML/Graphics.hpp>
#include <chrono>
#include <string>
#include <thread>

namespace plague {

inline void display(Board& board, int range, double bet, double gam, int n_ppl,
                    double start_lock, double end_lock, Graph& graph) {
  int n = board.size();
  if (n * 8 < 600) {
    n = 75;
  }
  sf::RenderWindow window(sf::VideoMode(8 * n + 950, 8 * n + 6),
                          "Virus simulation board - SIR model",
                          sf::Style::Close | sf::Style::Titlebar);

  int dim = board.size();
  constexpr float border_thickness = 3.f;
  constexpr float cell_side = 8.f;
  constexpr float person_radius = 3.f;
  constexpr float inner_space = 50.f;
  constexpr float axis_thickness = 3.f;
  constexpr float graph_points_side = 2.f;
  constexpr float top_space = 10.f;
  constexpr float y_height = 460.f;

  sf::Color light_grey(211, 211, 211);

  sf::RectangleShape right_border(
      sf::Vector2f(border_thickness, dim * cell_side + 2 * border_thickness));
  right_border.setPosition(
      sf::Vector2f(dim * cell_side + border_thickness, 0.f));
  right_border.setFillColor(light_grey);

  sf::RectangleShape bottom_border(
      sf::Vector2f(dim * cell_side + 2 * border_thickness, border_thickness));
  bottom_border.setPosition(
      sf::Vector2f(0.f, dim * cell_side + border_thickness));
  bottom_border.setFillColor(light_grey);

  sf::RectangleShape left_border(
      sf::Vector2f(border_thickness, dim * cell_side + border_thickness));
  left_border.setPosition(sf::Vector2f(0.f, 0.f));
  left_border.setFillColor(light_grey);

  sf::RectangleShape top_border(
      sf::Vector2f(dim * cell_side + border_thickness, border_thickness));
  top_border.setPosition(sf::Vector2f(0.f, 0.f));
  top_border.setFillColor(light_grey);

  sf::RectangleShape x_axis(
      sf::Vector2f(365 * graph_points_side + axis_thickness, axis_thickness));
  x_axis.setPosition(
      sf::Vector2f(dim * cell_side + inner_space, top_space + y_height));
  x_axis.setFillColor(light_grey);

  sf::RectangleShape y_axis(sf::Vector2f(axis_thickness, y_height));
  y_axis.setPosition(sf::Vector2f(dim * cell_side + inner_space, top_space));
  y_axis.setFillColor(light_grey);

  sf::RectangleShape sus_graph(
      sf::Vector2f(graph_points_side, graph_points_side));
  sus_graph.setFillColor(sf::Color::Yellow);

  sf::RectangleShape inf_graph(
      sf::Vector2f(graph_points_side, graph_points_side));
  inf_graph.setFillColor(sf::Color::Red);

  sf::RectangleShape rec_graph(
      sf::Vector2f(graph_points_side, graph_points_side));
  rec_graph.setFillColor(sf::Color::Blue);

  sf::Font font;
  font.loadFromFile("lcd2n.ttf");

  sf::Text n_sus;
  n_sus.setFont(font);
  n_sus.setPosition(
      sf::Vector2f(dim * cell_side + inner_space, y_height + top_space + 30.f));
  n_sus.setFillColor(sf::Color::Yellow);

  sf::Text n_inf;
  n_inf.setFont(font);
  n_inf.setPosition(
      sf::Vector2f(dim * cell_side + inner_space, y_height + top_space + 60.f));
  n_inf.setFillColor(sf::Color::Red);

  sf::Text n_rec;
  n_rec.setFont(font);
  n_rec.setPosition(
      sf::Vector2f(dim * cell_side + inner_space, y_height + top_space + 90.f));
  n_rec.setFillColor(sf::Color::Blue);

  sf::Text day;
  day.setFont(font);
  day.setPosition(sf::Vector2f(dim * cell_side + inner_space + 490.f,
                               top_space + y_height + 10.f));
  day.setFillColor(light_grey);
  day.setCharacterSize(65);

  sf::Text instructions;
  instructions.setFont(font);
  instructions.setFillColor(light_grey);
  instructions.setPosition(dim * cell_side + inner_space + 440.f,
                           top_space + y_height + 90.f);
  instructions.setString(std::string("hold space to pause"));

  int t = 0;
  int tot_inf = 1; // serve per farlo fermare se gli infetti diventano 0,
                   // inizializzato a 1 così almeno 1 tabella la disegna
  bool lockdown = false;

  while (window.isOpen()) {

    sf::Event event;
    while (window.pollEvent(event)) {
      if (event.type == sf::Event::Closed) {
        window.close();
        break;
      }
    }

    while (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Space)) {
      ;
    }

    if (t != 365 && tot_inf != 0) {

      for (int i = 0; i != dim; ++i) {
        for (int j = 0; j != dim; ++j) {
          sf::CircleShape person;
          person.setRadius(person_radius);
          person.setPosition(sf::Vector2f(i * cell_side + border_thickness,
                                          j * cell_side + border_thickness));
          switch (board.getState(i, j)) {
          case State::Empty: {
            person.setFillColor(sf::Color::Transparent);
            break;
          }
          case State::Susceptible: {
            person.setFillColor(sf::Color::Yellow);
            break;
          }
          case State::Infectious: {
            person.setFillColor(sf::Color::Red);
            break;
          }
          case State::Recovered: {
            person.setFillColor(sf::Color::Blue);
            break;
          }
          default:
            throw std::runtime_error{"invalid State"};
          }
          window.draw(person);
        }
      }
      graph.fill_graph(board, t);

      window.draw(right_border);
      window.draw(bottom_border);
      window.draw(left_border);
      window.draw(top_border);

      for (int k = 0; k != t + 1 && k != 365; ++k) {
        sus_graph.setPosition(sf::Vector2f(
            (dim * cell_side + inner_space + (k * graph_points_side) +
             axis_thickness),
            top_space + y_height -
                (static_cast<float>(graph.num_susceptible(k)) / (n_ppl)) *
                    y_height));
        window.draw(sus_graph);

        inf_graph.setPosition(sf::Vector2f(
            (dim * cell_side + inner_space + (k * graph_points_side) +
             axis_thickness),
            top_space + y_height -
                (static_cast<float>(graph.num_infectious(k)) / (n_ppl)) *
                    y_height));
        window.draw(inf_graph);

        rec_graph.setPosition(sf::Vector2f(
            (dim * cell_side + inner_space + (k * graph_points_side) +
             axis_thickness),
            top_space + y_height -
                (static_cast<float>(graph.num_recovered(k)) / (n_ppl)) *
                    y_height));
        window.draw(rec_graph);
      }

      window.draw(x_axis);
      window.draw(y_axis);

      n_sus.setString(std::string("num sus = ") +
                      std::to_string(graph.num_susceptible(t)));
      window.draw(n_sus);

      n_inf.setString(std::string("num inf = ") +
                      std::to_string(graph.num_infectious(t)));
      window.draw(n_inf);

      n_rec.setString(std::string("num rec = ") +
                      std::to_string(graph.num_recovered(t)));
      window.draw(n_rec);

      day.setString(std::string("day: ") + std::to_string(t));
      window.draw(day);
      window.draw(instructions);

      window.display();
      std::this_thread::sleep_for(std::chrono::milliseconds(3));

      tot_inf = graph.num_infectious(t); // per fermarlo se n_inf = 0
      evolve(board, lockdown, range, bet, gam, n_ppl, start_lock, end_lock,
             graph, t);
      window.clear();
      ++t;
    }
  }
}

} // namespace plague

#endif