#ifndef PLAGUE_INPUT_HPP
#define PLAGUE_INPUT_HPP

#include <iostream>
#include <thread>

namespace plague {
inline void enter_parameters(int& dim, int& range, int& n_ppl, int& n_inf,
                             int& n_rec, double& bet, double& gam,
                             double& start_lock, double& end_lock) {

  std::cout << "Enter infection range (must be an int): ";
  std::cin >> range;

  while (std::cin.fail() || range <= 0) {
    std::cin.clear();
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    std::cout << "Enter infection range (must be an int): ";
    std::cin >> range;
  };
  do {
    std::cin.clear();
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    std::cout << "Enter infection rate \u03B2 (between 0 and 1): ";
    std::cin >> bet;
  } while (std::cin.fail() || bet < 0. || bet > 1.);

  do {
    std::cin.clear();
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    std::cout << "Enter recovery rate \u03B3 (between 0 and 1): ";
    std::cin >> gam;
  } while (std::cin.fail() || gam < 0. || gam > 1.);

  do {
    std::cin.clear();
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    std::cout << "Enter side of the area of interest (max 90): ";
    std::cin >> dim;
  } while (std::cin.fail() || dim <= 0 || dim > 90);

  do {
    std::cin.clear();
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    std::cout << "Enter population size (max " << dim * dim
              << ", default will be susceptible): ";
    std::cin >> n_ppl;
  } while (std::cin.fail() || n_ppl < 0 || n_ppl > dim * dim);

  do {
    std::cin.clear();
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    std::cout << "Enter number of infectious individuals (max " << n_ppl
              << "): ";
    std::cin >> n_inf;
  } while (std::cin.fail() || n_inf < 0 || n_inf > n_ppl);

  do {
    std::cin.clear();
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    std::cout << "Enter number of immune individuals (max " << n_ppl - n_inf
              << "): ";
    std::cin >> n_rec;
  } while (std::cin.fail() || n_rec < 0 || n_rec > (n_ppl - n_inf));

  char lock;
  do {
    std::cin.clear();
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    std::cout << "Do you want to enable lockdown? [y/n] ";
    std::cin >> lock;
  } while (std::cin.fail() ||
           (lock != 'Y' && lock != 'y' && lock != 'N' && lock != 'n'));

  if (lock == 'y') {
    do {
      std::cin.clear();
      std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
      std::cout << "Enter start lockdown rate (between 0 and 1): ";
      std::cin >> start_lock;
    } while (std::cin.fail() || start_lock < 0. || start_lock > 1.);

    do {
      std::cin.clear();
      std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
      std::cout << "Enter end lockdown rate (between 0 and " << start_lock
                << "): ";
      std::cin >> end_lock;
    } while (std::cin.fail() || end_lock < 0. || end_lock >= start_lock);

  } else {
    start_lock = 1.1;
    end_lock = 0.;
  }
}
} // namespace plague

#endif