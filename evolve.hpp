#ifndef PLAGUE_EVOLVE_HPP
#define PLAGUE_EVOLVE_HPP

#include "board.hpp"
#include "graph.hpp"
#include <random>

namespace plague {

inline void single_movement(Board& newboard, Board const& board, int i, int j) {
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_int_distribution<> dis(1, 9);
  int g = dis(gen);
  int cont = 0;
  for (int k = i - 1; k != i + 2; ++k) {
    for (int l = j - 1; l != j + 2 && newboard(i, j) != State::Empty; ++l) {
      ++cont;
      if (board.getState(k, l) == State::Empty &&
          newboard.getState(k, l) == State::Empty) {
        if (g == cont) {
          newboard(k, l) = board.getState(i, j);
          newboard(i, j) = State::Empty;
        }
      }
    }
  }
}

inline void movement0(Board const& board, Board& newboard) {
  int dim = board.size();
  for (int i = 0; i != dim; ++i) {
    for (int j = 0; j != dim; ++j) {
      if (board.getState(i, j) != State::Empty &&
          newboard.getState(i, j) != State::Empty) {
        single_movement(newboard, board, i, j);
      }
    }
  }
}

inline void movement1(Board const& board, Board& newboard) {
  int dim = board.size();
  for (int j = dim - 1; j != -1; --j) {
    for (int i = 0; i != dim; ++i) {
      if (board.getState(i, j) != State::Empty &&
          newboard.getState(i, j) != State::Empty) {
        single_movement(newboard, board, i, j);
      }
    }
  }
}

inline void movement2(Board const& board, Board& newboard) {
  int dim = board.size();
  for (int i = dim - 1; i != -1; --i) {
    for (int j = dim - 1; j != -1; --j) {
      if (board.getState(i, j) != State::Empty &&
          newboard.getState(i, j) != State::Empty) {
        single_movement(newboard, board, i, j);
      }
    }
  }
}

inline void movement3(Board const& board, Board& newboard) {
  int dim = board.size();
  for (int j = 0; j != dim; ++j) {
    for (int i = dim - 1; i != -1; --i) {
      if (board.getState(i, j) != State::Empty &&
          newboard.getState(i, j) != State::Empty) {
        single_movement(newboard, board, i, j);
      }
    }
  }
}

inline Board collective_movement(Board const& board, int t) {
  Board newboard = board; // serve per evitare che uno si muova nello spazio
                          // appena lasciato libero oppure due volte
  switch (t % 4) {
  case 0: {
    movement0(board, newboard);
    break;
  }
  case 1: {
    movement1(board, newboard);
    break;
  }
  case 2: {
    movement2(board, newboard);
    break;
  }
  case 3: {
    movement3(board, newboard);
    break;
  }
  default:
    throw std::runtime_error("invalid time");
  }
  return newboard;
}

inline int nearby_infectious(Board const& board, int i, int j, int range) {
  int inf = 0;
  for (int k = i - range; k != i + 1 + range; ++k) {
    for (int l = j - range; l != j + 1 + range; ++l) {
      if (board.getState(k, l) == State::Infectious) {
        ++inf;
      }
    }
  }
  return inf;
}

inline Board change_of_state(Board const& board, int range, double bet,
                             double gam) {
  int dim = board.size();
  Board newboard = board;
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(0., 1.);
  for (int i = 0; i != dim; ++i) {
    for (int j = 0; j != dim; ++j) {
      int inf = nearby_infectious(board, i, j, range);
      for (int k = 0; k != inf && newboard.getState(i, j) == State::Susceptible;
           ++k) {
        if (dis(gen) <= bet) {
          newboard(i, j) = State::Infectious;
        };
      }
      if (board.getState(i, j) == State::Infectious) {
        if (dis(gen) <= gam) {
          newboard(i, j) = State::Recovered;
        }
      }
    }
  }
  return newboard;
}

inline void evolve(Board& board, bool& lockdown, int range, double bet,
                   double gam, int n_ppl, double start_lock, double end_lock,
                   Graph const& graph, int t) {
  board = change_of_state(board, range, bet, gam);
  double n_inf = graph.num_infectious(t);
  lockdown == true ? lockdown = (n_inf / n_ppl >= end_lock)
                   : lockdown = (n_inf / n_ppl >= start_lock);
  if (lockdown == false) {
    board = collective_movement(board, t);
  }
}
} // namespace plague
#endif