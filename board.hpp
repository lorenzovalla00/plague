#ifndef PLAGUE_BOARD_HPP
#define PLAGUE_BOARD_HPP

#include <algorithm>
#include <cassert>
#include <iostream>
#include <iterator>
#include <random>
#include <vector>

namespace plague {

enum class State : int { Empty, Susceptible, Infectious, Recovered };

class Board {
private:
  int n_;
  std::vector<State> board_;

public:
  Board() : n_{0}, board_() {}
  Board(int n) : n_{n}, board_(n_ * n_, State::Empty) {}
  Board(int n, State state) : n_{n}, board_(n_ * n_, state) {}

  int size() const { return n_; }

  State& operator()(int i, int j) {
    assert(i >= 0 && i < n_ && j >= 0 && j < n_);
    return board_[i * n_ + j];
  }

  State getState(int i, int j) const {
    return (i >= 0 && i < n_ && j >= 0 && j < n_ ? board_[i * n_ + j]
                                                 : State::Recovered);
  }

  void random(Board& board, int dim, int n_ppl, int n_inf, int n_rec) {
    board.n_ = dim;
    for (int i = 0; i != dim * dim; ++i) {
      if (i < n_rec) {
        (board.board_).push_back(State::Recovered);
      } else if (i < (n_rec + n_inf)) {
        (board.board_).push_back(State::Infectious);
      } else if (i < (n_ppl)) {
        (board.board_).push_back(State::Susceptible);
      } else {
        (board.board_).push_back(State::Empty);
      }
    }
    std::random_device rd;
    std::mt19937 g(rd());
    std::shuffle((board.board_).begin(), (board.board_).end(), g);
  }
};

} // namespace plague

#endif
