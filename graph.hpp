#ifndef PLAGUE_GRAPH_HPP
#define PLAGUE_GRAPH_HPP

#include "board.hpp"
#include <SFML/Graphics.hpp>
#include <string>

namespace plague {
class Graph {
private:
  std::vector<int> susceptible_;
  std::vector<int> infectious_;
  std::vector<int> recovered_;

public:
  Graph() : susceptible_(), infectious_(), recovered_() {}

  int num_susceptible(int t) const { return susceptible_[t]; }

  int num_infectious(int t) const { return infectious_[t]; }

  int num_recovered(int t) const { return recovered_[t]; }

  void fill_graph(Board const& board, int t) {
    int dim = board.size();
    susceptible_.push_back(0);
    infectious_.push_back(0);
    recovered_.push_back(0);
    for (int i = 0; i != dim; ++i) {
      for (int j = 0; j != dim; ++j) {
        switch (board.getState(i, j)) {
        case State::Empty: {
          break;
        }
        case State::Susceptible: {
          ++susceptible_[t];
          break;
        }
        case State::Infectious: {
          ++infectious_[t];
          break;
        }
        case State::Recovered: {
          ++recovered_[t];
          break;
        }
        default:
          throw std::runtime_error("invalid State");
        }
      }
    }
  }
};
} // namespace plague
#endif