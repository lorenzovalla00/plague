
#include "board.hpp"
#include "display.hpp"
#include "graph.hpp"
#include "input.hpp"

int main() {
  int dim;
  int range;
  int n_ppl, n_inf, n_rec;
  double bet, gam;
  double start_lock, end_lock;
  plague::enter_parameters(dim, range, n_ppl, n_inf, n_rec, bet, gam,
                           start_lock, end_lock);
  plague::Board board;
  plague::Graph graph;
  board.random(board, dim, n_ppl, n_inf, n_rec);
  plague::display(board, range, bet, gam, n_ppl, start_lock, end_lock, graph);
}