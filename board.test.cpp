#include "board.hpp"
#include "doctest.h"

namespace plague {
TEST_CASE("testing Board") {
  Board board1(10);
  CHECK(board1.getState(-1, 3) == State::Recovered);
  CHECK(board1.getState(12, 4) == State::Recovered);
  CHECK(board1.size() == 10);

  Board board2(67, State::Susceptible);
  for (int i = 0; i != 67; ++i) {
    for (int j = 0; j != 67; ++j) {
      CHECK(board2.getState(i, j) == State::Susceptible);
    }
  }
  CHECK(board2.getState(-1, 17) == State::Recovered);
  CHECK(board2.getState(78, 20) == State::Recovered);
  CHECK(board2.size() == 67);

  Board board3;
  CHECK(board3.size() == 0);
  board3.random(board3, 30, 900, 30, 13);
  CHECK(board3.getState(22, 17) != State::Empty);
  CHECK(board3.getState(15, 2) != State::Empty);
  
  Board board4(30);
  board4.random(board4, 70, 1000, 600, 400);
  CHECK(board4.size() == 70);
  CHECK(board4.getState(50, 35) != State::Susceptible);
  CHECK(board4.getState(2, 29) != State::Susceptible);
  CHECK(board4.getState(71, 44) == State::Recovered);
}
} // namespace plague