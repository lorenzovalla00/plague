Per poter eseguire il programma è prima necessario buildarlo, oltre a possedere SFML e cmake. Per fare ciò, per prima cosa bisogna scaricare la cartella da terminale, attraverso il comando

git clone https://gitlab.com/lorenzovalla00/plague.git

Bisogna poi generare la configurazione di build, attraverso i comandi:

mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Debug ..

Successivamente è possibile runnare la build con il comando:

cmake --build .

Prima di poter avviare l'eseguibile è necessario inserire una copia del file lcd2n.ttf, contenente il font, nella cartella build. Per fare ciò, tornare nella cartella plague, dove si trova il file, e digitare:

cp lcd2n.ttf build

Dopo aver fatto ciò, tornare nell cartella build tramite cd build; da qui è possibile avviare il programma digitando:

./plague

N.B. per poter visualizzare la finestra grafica di SFML è necessario, se non si possiedono altri strumenti, avere installato un programma XServer (ad esempio MobaXTerm)

I test sono buildati da CMake insieme al programma principale. Per runnare tutti i test è possibile digitare, dalla cartella build, il comando:

ctest

