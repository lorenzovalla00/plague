#include "board.hpp"
#include "display.hpp"
#include "doctest.h"
#include "graph.hpp"
#include <SFML/Graphics.hpp>
namespace plague {
TEST_CASE("testing Graph") {
  Board board1;
  board1.random(board1, 80, 4000, 10, 70);
  Graph graph1;
  graph1.fill_graph(board1, 0);
  CHECK(graph1.num_infectious(0) == 10);
  CHECK(graph1.num_susceptible(0) == 3920);
  CHECK(graph1.num_recovered(0) == 70);

  Board board2;
  board2.random(board2, 90, 8100, 90, 100);
  Graph graph2;
  graph2.fill_graph(board2, 0);
  CHECK(graph2.num_infectious(0) == 90);
  CHECK(graph2.num_susceptible(0) == 7910);
  CHECK(graph2.num_recovered(0) == 100);
};
} // namespace plague