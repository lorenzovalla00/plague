#include "display.hpp"
#include "doctest.h"

namespace plague {
TEST_CASE("testing Display") {
  constexpr float border_thickness = 3.f;
  constexpr float cell_side = 8.f;
  constexpr float person_radius = 3.f;
  constexpr float inner_space = 50.f;
  constexpr float axis_thickness = 3.f;
  constexpr float graph_points_side = 2.f;
  constexpr float top_space = 10.f;
  constexpr float y_height = 460.f;
  
  CHECK(cell_side == 8.f);
  CHECK(person_radius == 3.f);
  CHECK(inner_space == 50.f);
  CHECK(axis_thickness == 3.f);
  CHECK(graph_points_side == 2.f);
  CHECK(top_space == 10.f);
  CHECK(y_height == 460.f);
}
} // namespace plague